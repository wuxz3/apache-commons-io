Name:           apache-commons-io
Epoch:          1
Version:        2.6
Release:        9
Summary:        A library of utilities for developing IO functionality.
License:        ASL 2.0
URL:            http://commons.apache.org/proper/commons-io
Source0:        http://archive.apache.org/dist/commons/io/source/commons-io-%{version}-src.tar.gz
Patch0000:      CVE-2021-29425-1.patch
Patch0001:      CVE-2021-29425-2.patch
Patch0002:      CVE-2021-29425-3.patch
Patch0003:      CVE-2021-29425-4.patch
BuildArch:      noarch
BuildRequires:  mvn(org.apache.maven.plugins:maven-antrun-plugin) maven-local >= 5.3.0-3
BuildRequires:  mvn(org.apache.commons:commons-parent:pom:) mvn(junit:junit)

%description
Apache commons IO library is used for developing IO functionality. It contains a collecton of utilities with
utility classes, stream implementations, file filters, file comparators, endian transformation classes, and much more.

%package        help
Summary:        Help documents for apache-commons-io
Provides:       %{name}-javadoc = %{version}-%{release}
Obsoletes:      %{name}-javadoc < %{version}-%{release}

%description    help
Help documents for apache-commons-io.

%prep
%autosetup -n commons-io-%{version}-src -p1
# remove <scope>test</scope>
%pom_xpath_remove "pom:dependency[pom:artifactId='junit']/pom:scope"

%build
%mvn_file  : commons-io %{name}
%mvn_alias : org.apache.commons:
%mvn_build --skipTests

%install
%mvn_install

%check
xmvn test --batch-mode --offline verify

%files          -f .mfiles
%license LICENSE.txt NOTICE.txt

%files          help -f .mfiles-javadoc
%doc RELEASE-NOTES.txt

%changelog
* Fri Dec 30 2022 wuxiangzhao <wuxz3@chinatelecom.cn> - 1:2.6-9
- Add release requirements for maven-local

* Fri Sep 23 2022 yaoxin <yaoxin30@h-partners.com> - 1:2.6-8
- Remove the empty Ignore-some-test-because-bep.patch file.

* Sat May 8 2021 wangxiao <wangxiao65@huawei.com> - 1:2.6-7
- Fix CVE-2021-29425

* Mon Apr 26 2021 maminjie <maminjie1@huawei.com> - 1:2.6-6
- Move the test to the %check stage

* Thu Dec 5 2019 chenzhenyu <chenzhenyu13@huawei.com> - 1:2.6-5
- Package init
